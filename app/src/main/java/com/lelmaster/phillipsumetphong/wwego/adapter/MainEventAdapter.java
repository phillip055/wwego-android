package com.lelmaster.phillipsumetphong.wwego.adapter;

import android.content.Context;

import com.lelmaster.phillipsumetphong.wwego.manager.MainEventManager;

/**
 * Created by vorathepsumetphong on 9/1/16.
 */
public class MainEventAdapter extends AllAdapter {
    public MainEventAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemCount() {
        if (MainEventManager.getInstance().getShowCollectionDao()==null){
            return 0;
        }
        return MainEventManager.getInstance().getShowCollectionDao().getShowDaos().size();
    }

    public Object getItem(int position) {
        return MainEventManager.getInstance().getShowCollectionDao().getShowDaos().get(position);
    }
}
