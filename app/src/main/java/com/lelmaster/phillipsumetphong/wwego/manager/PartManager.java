package com.lelmaster.phillipsumetphong.wwego.manager;

import android.content.Context;

import com.lelmaster.phillipsumetphong.wwego.dao.PartialLinkDao;

/**
 * Created by vorathepsumetphong on 8/28/16.
 */
public class PartManager {
    private static PartManager instance;

    private PartialLinkDao partialLinkDao;


    public static PartManager getInstance() {
        if (instance == null)
            instance = new PartManager();
        return instance;
    }

    private Context mContext;

    public PartialLinkDao getPartialLinkDao() {
        return partialLinkDao;
    }

    public void setPartialLinkDao(PartialLinkDao partialLinkDao) {
        this.partialLinkDao = partialLinkDao;
    }
}
