package com.lelmaster.phillipsumetphong.wwego.manager;

import android.content.Context;

import com.lelmaster.phillipsumetphong.wwego.dao.ShowCollectionDao;
import com.lelmaster.phillipsumetphong.wwego.dao.ShowDao;

/**
 * Created by vorathepsumetphong on 8/27/16.
 */
public class PPVManager implements ShowManager  {
    private static PPVManager instance;

    private ShowDao showDao;
    private ShowCollectionDao showCollectionDao;


    public static PPVManager getInstance() {
        if (instance == null)
            instance = new PPVManager();
        return instance;
    }

    private Context mContext;

    public ShowCollectionDao getShowCollectionDao() {
        return showCollectionDao;
    }

    public void addShowCollectionDao(ShowCollectionDao showCollectionDao){
        this.showCollectionDao.getShowDaos().addAll(showCollectionDao.getShowDaos());
    }

    public ShowDao getShowDao() {
        return showDao;
    }

    public void setShowCollectionDao(ShowCollectionDao showCollectionDao) {
        this.showCollectionDao = showCollectionDao;
    }

    public void setShowDao(ShowDao showDao) {
        this.showDao = showDao;
    }
}
