package com.lelmaster.phillipsumetphong.wwego.adapter;

import android.content.Context;

import com.lelmaster.phillipsumetphong.wwego.manager.RawManager;

/**
 * Created by vorathepsumetphong on 9/1/16.
 */
public class RawAdapter extends AllAdapter {
    public RawAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemCount() {
        if (RawManager.getInstance().getShowCollectionDao()==null){
            return 0;
        }
        return RawManager.getInstance().getShowCollectionDao().getShowDaos().size();
    }

    public Object getItem(int position) {
        return RawManager.getInstance().getShowCollectionDao().getShowDaos().get(position);
    }

}
