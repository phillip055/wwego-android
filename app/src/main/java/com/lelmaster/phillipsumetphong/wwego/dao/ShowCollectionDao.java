package com.lelmaster.phillipsumetphong.wwego.dao;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vorathepsumetphong on 8/27/16.
 */
public class ShowCollectionDao {
    @SerializedName("shows")
    private List<ShowDao> showDaos;

    @SerializedName("count")
    private int count;

    public List<ShowDao> getShowDaos() {
        return showDaos;
    }

    public void setShowDaos(List<ShowDao> showDaos) {
        this.showDaos = showDaos;
    }
}
