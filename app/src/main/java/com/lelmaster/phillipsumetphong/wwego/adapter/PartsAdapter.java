package com.lelmaster.phillipsumetphong.wwego.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.lelmaster.phillipsumetphong.wwego.dao.PartialLinkDao;
import com.lelmaster.phillipsumetphong.wwego.manager.AllManager;
import com.lelmaster.phillipsumetphong.wwego.view.PartLinkListItem;

/**
 * Created by vorathepsumetphong on 8/28/16.
 */
public class PartsAdapter extends BaseAdapter {
    @Override
    public int getCount() {
        if (AllManager.getInstance().getShowDao()==null){
            return 0;
        }
        return AllManager.getInstance().getShowDao().getPartialLinkDaos().size();
    }

    @Override
    public Object getItem(int position) {
        return AllManager.getInstance().getShowDao().getPartialLinkDaos().get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PartLinkListItem item;
        if (convertView != null){
            item = (PartLinkListItem) convertView;
        }else{
            item = new PartLinkListItem(parent.getContext());
        }
        PartialLinkDao dao = (PartialLinkDao) getItem(position);
        String string;
        if (dao.isWorking()){
            item.setIsWorking("");
        }
        else {
            item.setIsWorking("(Content Removed!)");
        }
        item.setName("  " + dao.getName());
        return item;
    }
}
