package com.lelmaster.phillipsumetphong.wwego.adapter;

import android.content.Context;

import com.lelmaster.phillipsumetphong.wwego.manager.UFCManager;

/**
 * Created by vorathepsumetphong on 9/1/16.
 */
public class UFCAdapter extends AllAdapter {
    public UFCAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemCount() {
        if (UFCManager.getInstance().getShowCollectionDao()==null){
            return 0;
        }
        return UFCManager.getInstance().getShowCollectionDao().getShowDaos().size();
    }

    public Object getItem(int position) {
        return UFCManager.getInstance().getShowCollectionDao().getShowDaos().get(position);
    }
}
