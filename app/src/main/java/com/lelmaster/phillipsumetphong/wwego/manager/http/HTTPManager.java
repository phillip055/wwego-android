package com.lelmaster.phillipsumetphong.wwego.manager.http;

import android.content.Context;

import com.lelmaster.phillipsumetphong.wwego.manager.Contextor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import retrofit.Retrofit;
import retrofit.GsonConverterFactory;

/**
 * Created by vorathepsumetphong on 8/27/16.
 */
public class HTTPManager {
    private static HTTPManager instance;

    public static HTTPManager getInstance() {
        if (instance == null)
            instance = new HTTPManager();
        return instance;
    }

    private Context mContext;
    private APIService mService;
    private HTTPManager() {

        mContext = Contextor.getInstance().getContext();

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        OkHttpClient httpClient = new OkHttpClient();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://45.55.158.15/wwe/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient)
                .build();
        mService = retrofit.create(APIService.class);
    }

    public APIService getService(){
        return mService;
    }
}
