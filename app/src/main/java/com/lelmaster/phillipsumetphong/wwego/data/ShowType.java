package com.lelmaster.phillipsumetphong.wwego.data;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.widget.BaseAdapter;

import com.lelmaster.phillipsumetphong.wwego.adapter.AllAdapter;
import com.lelmaster.phillipsumetphong.wwego.adapter.MainEventAdapter;
import com.lelmaster.phillipsumetphong.wwego.adapter.NXTAdapter;
import com.lelmaster.phillipsumetphong.wwego.adapter.RawAdapter;
import com.lelmaster.phillipsumetphong.wwego.adapter.SmackDownAdapter;
import com.lelmaster.phillipsumetphong.wwego.adapter.TNCAdapter;
import com.lelmaster.phillipsumetphong.wwego.adapter.UFCAdapter;
import com.lelmaster.phillipsumetphong.wwego.manager.AllManager;
import com.lelmaster.phillipsumetphong.wwego.manager.MainEventManager;
import com.lelmaster.phillipsumetphong.wwego.manager.NXTManager;
import com.lelmaster.phillipsumetphong.wwego.manager.RawManager;
import com.lelmaster.phillipsumetphong.wwego.manager.ShowManager;
import com.lelmaster.phillipsumetphong.wwego.manager.SmackDownManager;
import com.lelmaster.phillipsumetphong.wwego.manager.TNCManager;
import com.lelmaster.phillipsumetphong.wwego.manager.UFCManager;

/**
 * Created by vorathepsumetphong on 8/27/16.
 */
public enum ShowType {
    SmackDown,Raw,PPV,Main_Event,NXT,TNC,UFC,ALL;

    public ShowManager getManager(){
        switch (this){
            case SmackDown:
                return SmackDownManager.getInstance();
            case Raw:
                return RawManager.getInstance();
            case TNC:
                return TNCManager.getInstance();
            case NXT:
                return NXTManager.getInstance();
            case UFC:
                return UFCManager.getInstance();
            case Main_Event:
                return MainEventManager.getInstance();
            default:
                return AllManager.getInstance();
        }
    }

    public RecyclerView.Adapter getAdapter(Context context){
        switch (this){
            case SmackDown:
                return new SmackDownAdapter(context);
            case Raw:
                return new RawAdapter(context);
            case TNC:
                return new TNCAdapter(context);
            case NXT:
                return new NXTAdapter(context);
            case UFC:
                return new UFCAdapter(context);
            case Main_Event:
                return new MainEventAdapter(context);
            default:
                return new AllAdapter(context);
        }
    }
}
