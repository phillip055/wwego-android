package com.lelmaster.phillipsumetphong.wwego;

import android.app.Application;

import com.lelmaster.phillipsumetphong.wwego.manager.Contextor;
import com.parse.Parse;
import com.parse.ParseInstallation;

/**
 * Created by vorathepsumetphong on 8/27/16.
 */
public class MainApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Contextor.getInstance().init(getApplicationContext());
        Parse.initialize(this, "eqPUOOUIm1Mg7U4I1z40wrJSlk7yidsRTRbJZpEM", "whAd6W0Pz32BOU3ENp3FBJzlg4wYtOZxDpAJUzNo");
        ParseInstallation.getCurrentInstallation().saveInBackground();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
