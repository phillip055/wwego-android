package com.lelmaster.phillipsumetphong.wwego.manager.http;

import com.lelmaster.phillipsumetphong.wwego.dao.ShowCollectionDao;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by vorathepsumetphong on 8/27/16.
 */
public interface APIService {
    @GET("all")
    Call<ShowCollectionDao> all(@Query("offset") int offset,@Query("limit") int limit);

    @GET("ufc")
    Call<ShowCollectionDao> ufc(@Query("offset") int offset,@Query("limit") int limit);

    @GET("tna")
    Call<ShowCollectionDao> tna(@Query("offset") int offset,@Query("limit") int limit);

    @GET("raw")
    Call<ShowCollectionDao> raw(@Query("offset") int offset,@Query("limit") int limit);

    @GET("smackdown")
    Call<ShowCollectionDao> smack_down(@Query("offset") int offset,@Query("limit") int limit);

    @GET("main_event")
    Call<ShowCollectionDao> main_event(@Query("offset") int offset,@Query("limit") int limit);

    @GET("nxt")
    Call<ShowCollectionDao> nxt(@Query("offset") int offset,@Query("limit") int limit);

    @GET("ppv")
    Call<ShowCollectionDao> ppv(@Query("offset") int offset,@Query("limit") int limit);
}
