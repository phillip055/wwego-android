package com.lelmaster.phillipsumetphong.wwego.util;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by vorathepsumetphong on 8/30/16.
 */
public class TimeUtils {
    public static DateTime StringToDateTime(String datetime){
        DateTimeFormatter formatter = DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss z");
        DateTime dt = formatter.parseDateTime(datetime);
        return dt;
    }

    public static String StringToDisplayString(String datetime){
        DateTime dt = StringToDateTime(datetime);
        return String.valueOf(dt.dayOfMonth().get()) + "/" + String.valueOf(dt.monthOfYear().get()) + "/" + String.valueOf(dt.year().get());
    }
}
