package com.lelmaster.phillipsumetphong.wwego.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.lelmaster.phillipsumetphong.wwego.activity.ShowActivity;
import com.lelmaster.phillipsumetphong.wwego.dao.ShowDao;
import com.lelmaster.phillipsumetphong.wwego.manager.AllManager;
import com.lelmaster.phillipsumetphong.wwego.view.ShowGridItem;

import static com.lelmaster.phillipsumetphong.wwego.util.TimeUtils.StringToDisplayString;

/**
 * Created by vorathepsumetphong on 8/27/16.
 */
public class AllAdapter extends RecyclerView.Adapter<AllAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ShowGridItem item;
        public ViewHolder (ShowGridItem showGridItem){
            super(showGridItem);
            showGridItem.setOnClickListener(this);
            this.item = showGridItem;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), ShowActivity.class);
            AllManager.getInstance().setShowDao(AllManager.getInstance().getShowCollectionDao().getShowDaos().get(this.getLayoutPosition()));
            v.getContext().startActivity(intent);
        }
    }

    public static Context mContext;

    public AllAdapter(Context context){
        mContext = context;
    }

    @Override
    public AllAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ShowGridItem v = new ShowGridItem(parent.getContext());
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(AllAdapter.ViewHolder holder, int position) {
        ShowDao showDao =(ShowDao) getItem(position);
        holder.item.setImageView(showDao.getCover_picture());
        holder.item.setName(showDao.getName());
        holder.item.setDate(StringToDisplayString(showDao.getCreated_date()));
    }



    @Override
    public int getItemCount() {
        if (AllManager.getInstance().getShowCollectionDao()==null){
            return 0;
        }
        return AllManager.getInstance().getShowCollectionDao().getShowDaos().size();
    }

    public Object getItem(int position) {
        return AllManager.getInstance().getShowCollectionDao().getShowDaos().get(position);
    }
}
