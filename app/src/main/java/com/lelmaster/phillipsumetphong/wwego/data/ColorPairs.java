package com.lelmaster.phillipsumetphong.wwego.data;

/**
 * Created by vorathepsumetphong on 8/31/16.
 */
public class ColorPairs<T,Y> {
    public T first;
    public Y second;
    public ColorPairs(T f, Y s)
    {
        first = f;
        second = s;
    }
}
