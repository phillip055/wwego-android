package com.lelmaster.phillipsumetphong.wwego.adapter;

import android.content.Context;

import com.lelmaster.phillipsumetphong.wwego.manager.NXTManager;

/**
 * Created by vorathepsumetphong on 9/1/16.
 */
public class NXTAdapter extends AllAdapter {
    public NXTAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemCount() {
        if (NXTManager.getInstance().getShowCollectionDao()==null){
            return 0;
        }
        return NXTManager.getInstance().getShowCollectionDao().getShowDaos().size();
    }

    public Object getItem(int position) {
        return NXTManager.getInstance().getShowCollectionDao().getShowDaos().get(position);
    }
}
