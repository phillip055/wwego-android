package com.lelmaster.phillipsumetphong.wwego.adapter;

import android.content.Context;

import com.lelmaster.phillipsumetphong.wwego.manager.TNCManager;

/**
 * Created by vorathepsumetphong on 9/1/16.
 */
public class TNCAdapter extends AllAdapter {
    public TNCAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemCount() {
        if (TNCManager.getInstance().getShowCollectionDao()==null){
            return 0;
        }
        return TNCManager.getInstance().getShowCollectionDao().getShowDaos().size();
    }

    public Object getItem(int position) {
        return TNCManager.getInstance().getShowCollectionDao().getShowDaos().get(position);
    }
}
