package com.lelmaster.phillipsumetphong.wwego.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.lelmaster.phillipsumetphong.wwego.R;
import com.lelmaster.phillipsumetphong.wwego.data.ShowType;
import com.lelmaster.phillipsumetphong.wwego.fragment.HomeFragment;
import com.lelmaster.phillipsumetphong.wwego.manager.AllManager;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private NavigationView navigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        setFragment(HomeFragment.newInstance(ShowType.ALL));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void setFragment(Fragment fragment){
        AllManager.getInstance().setShowCollectionDao(null);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, fragment).commit();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        navigationView.setCheckedItem(id);
        if (id == R.id.nav_raw){
            Toast.makeText(MainActivity.this, "WHAT", Toast.LENGTH_SHORT).show();
            setFragment(HomeFragment.newInstance(ShowType.Raw));
        }
        else if (id == R.id.nav_smackdown){
            setFragment(HomeFragment.newInstance(ShowType.SmackDown));
        }
        else if (id == R.id.nav_nxt){
            setFragment(HomeFragment.newInstance(ShowType.NXT));
        }
        else if (id == R.id.nav_tna){
            setFragment(HomeFragment.newInstance(ShowType.TNC));
        }
        else if (id == R.id.nav_home){
            setFragment(HomeFragment.newInstance(ShowType.ALL));
        }
        else if (id == R.id.nav_ufc){
            setFragment(HomeFragment.newInstance(ShowType.UFC));
        }
        else if(id== R.id.nav_ppv){
            setFragment(HomeFragment.newInstance(ShowType.PPV));
        }
        else if(id == R.id.nav_main_event){
            setFragment(HomeFragment.newInstance(ShowType.Main_Event));
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
