package com.lelmaster.phillipsumetphong.wwego.fragment;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.ads.InterstitialAd;
import com.lelmaster.phillipsumetphong.wwego.R;
import com.lelmaster.phillipsumetphong.wwego.adapter.PartsAdapter;
import com.lelmaster.phillipsumetphong.wwego.dao.PartialLinkDao;
import com.lelmaster.phillipsumetphong.wwego.manager.PartManager;
import com.lelmaster.phillipsumetphong.wwego.manager.AllManager;
import com.lelmaster.phillipsumetphong.wwego.view.DMWebVideoView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import static com.lelmaster.phillipsumetphong.wwego.util.TimeUtils.StringToDisplayString;

/**
 * Created by vorathepsumetphong on 8/28/16.
 */
public class ShowFragment extends Fragment {
    private ImageView imageView;
    private TextView date;
    private TextView name;
    private PartsAdapter adapter;
    private ListView parts_list;
    private boolean isShowing = false;
    private DMWebVideoView dmWebVideoView;
    private TextView part_name;
    InterstitialAd mInterstitialAd;

    public static ShowFragment newInstance() {
        Bundle args = new Bundle();
        ShowFragment fragment = new ShowFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.show_fragment, container, false);
        initInstances(rootView);
        mInterstitialAd = new InterstitialAd(getActivity());
        mInterstitialAd.setAdUnitId("ca-app-pub-5725454227850701/1146766271");
        return rootView;
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("DDCAABA9985E86757B37F06FCACF3633")
                .build();
        mInterstitialAd.loadAd(adRequest);
        mInterstitialAd.show();
    }

    private void initInstances(View rootView){
        MobileAds.initialize(getActivity(), "ca-app-pub-5725454227850701~6414570676");
        AdView mAdView = (AdView) rootView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("DDCAABA9985E86757B37F06FCACF3633")
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mAdView.loadAd(adRequest);
        imageView = (ImageView) rootView.findViewById(R.id.imageView2);
        part_name = (TextView) rootView.findViewById(R.id.watching_part);
        name = (TextView) rootView.findViewById(R.id.name);
        dmWebVideoView = (DMWebVideoView) rootView.findViewById(R.id.player);
        dmWebVideoView.setVisibility(View.INVISIBLE);
        date = (TextView) rootView.findViewById(R.id.date);
        parts_list = (ListView) rootView.findViewById(R.id.parts_list);
        adapter = new PartsAdapter();
        parts_list.setAdapter(adapter);
        parts_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                if (isShowing){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Are you sure you want to change what you are watching?")
                            .setNegativeButton("Cancel",null)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    setPart(position);
                                }
                            });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else {
                    setPart(position);
                }

            }
        });
        Glide.with(getContext())
                .load(AllManager.getInstance().getShowDao().getCover_picture())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
        name.setText(AllManager.getInstance().getShowDao().getName());
        date.setText(StringToDisplayString(AllManager.getInstance().getShowDao().getCreated_date()));
    }

    private void setPart(int position) {
        PartialLinkDao partialLinkDao = AllManager.getInstance().getShowDao().getPartialLinkDaos().get(position);
        if (!partialLinkDao.isWorking()){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Sorry, it's deleted..")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            AlertDialog dialog = builder.create();
            dialog.show();
            return;
        }
        part_name.setText(AllManager.getInstance().getShowDao().getPartialLinkDaos().get(position).getName());
        dmWebVideoView.setVisibility(View.VISIBLE);
        requestNewInterstitial();
        PartManager.getInstance().setPartialLinkDao(AllManager.getInstance().getShowDao().getPartialLinkDaos().get(position));

        isShowing = true;
        dmWebVideoView.setAutoPlay(true);
        String[] parts = PartManager.getInstance().getPartialLinkDao().getLink_url().split("/");
        String video_id = parts[parts.length-1];

        dmWebVideoView.setVideoId(video_id);
        String url = dmWebVideoView.load();
        Log.d("Video", url);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            dmWebVideoView.onPause();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            dmWebVideoView.onPause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            dmWebVideoView.onResume();
        }
        WindowManager mWindowManager = (WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE);
        Display mDisplay = mWindowManager.getDefaultDisplay();
        if (mDisplay.getRotation() != Surface.ROTATION_0){
            Toast.makeText(getContext(), "90", Toast.LENGTH_SHORT).show();
        }
    }
}
