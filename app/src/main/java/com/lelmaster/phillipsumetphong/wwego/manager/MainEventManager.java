package com.lelmaster.phillipsumetphong.wwego.manager;

import android.content.Context;

import com.lelmaster.phillipsumetphong.wwego.dao.ShowCollectionDao;
import com.lelmaster.phillipsumetphong.wwego.dao.ShowDao;

/**
 * Created by vorathepsumetphong on 9/1/16.
 */
public class MainEventManager implements ShowManager  {
    private static MainEventManager instance;

    private ShowDao showDao;
    private ShowCollectionDao showCollectionDao;


    public static MainEventManager getInstance() {
        if (instance == null)
            instance = new MainEventManager();
        return instance;
    }

    private Context mContext;

    public ShowCollectionDao getShowCollectionDao() {
        return showCollectionDao;
    }

    public void addShowCollectionDao(ShowCollectionDao showCollectionDao){
        this.showCollectionDao.getShowDaos().addAll(showCollectionDao.getShowDaos());
    }

    public ShowDao getShowDao() {
        return showDao;
    }

    public void setShowCollectionDao(ShowCollectionDao showCollectionDao) {
        this.showCollectionDao = showCollectionDao;
    }

    public void setShowDao(ShowDao showDao) {
        this.showDao = showDao;
    }
}
