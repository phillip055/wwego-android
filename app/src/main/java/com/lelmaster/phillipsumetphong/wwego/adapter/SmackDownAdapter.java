package com.lelmaster.phillipsumetphong.wwego.adapter;

import android.content.Context;

import com.lelmaster.phillipsumetphong.wwego.manager.SmackDownManager;

/**
 * Created by vorathepsumetphong on 9/1/16.
 */
public class SmackDownAdapter extends AllAdapter {
    public SmackDownAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemCount() {
        if (SmackDownManager.getInstance().getShowCollectionDao()==null){
            return 0;
        }
        return SmackDownManager.getInstance().getShowCollectionDao().getShowDaos().size();
    }

    public Object getItem(int position) {
        return SmackDownManager.getInstance().getShowCollectionDao().getShowDaos().get(position);
    }
}
