package com.lelmaster.phillipsumetphong.wwego.view;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lelmaster.phillipsumetphong.wwego.R;

/**
 * Created by vorathepsumetphong on 8/28/16.
 */
public class PartLinkListItem extends RelativeLayout {
    private TextView partName;
    private TextView isWorking;

    public PartLinkListItem(Context context) {
        super(context);
        initInflate();
        initInstances();
    }

    public PartLinkListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInstances();
        initWithAttrs(attrs);
    }

    public PartLinkListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInstances();
        initWithAttrs(attrs);
    }

    private void initInflate() {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.part_list_item, this);

    }

    private void initInstances() {
        partName = (TextView) findViewById(R.id.name);
        isWorking = (TextView) findViewById(R.id.isWorking);
    }

    private void initWithAttrs(AttributeSet attrs) {

    }

    public void setIsWorking(String isWorking) {
        this.isWorking.setText(isWorking);
    }

    public String getIsWorking() {
        return isWorking.getText().toString();
    }

    public void setName(String name){
        partName.setText(name);
    }
}
