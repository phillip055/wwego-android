package com.lelmaster.phillipsumetphong.wwego.dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vorathepsumetphong on 8/27/16.
 */
public class PartialLinkDao {
    @SerializedName("link_url")
    private String link_url;

    @SerializedName("name")
    private String name;

    @SerializedName("isWorking")
    private boolean isWorking;
//    @SerializedName("updated_at")
//    private String updated_at;


    public boolean isWorking() {
        return isWorking;
    }

    public void setWorking(boolean working) {
        isWorking = working;
    }

    public String getLink_url() {
        return link_url;
    }

    public void setLink_url(String link_url) {
        this.link_url = link_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public String getUpdated_at() {
//        return updated_at;
//    }

//    public void setUpdated_at(String updated_at) {
//        this.updated_at = updated_at;
//    }


}
