package com.lelmaster.phillipsumetphong.wwego.manager;

import com.lelmaster.phillipsumetphong.wwego.dao.ShowCollectionDao;
import com.lelmaster.phillipsumetphong.wwego.dao.ShowDao;

/**
 * Created by vorathepsumetphong on 9/1/16.
 */
public interface ShowManager {
    public ShowCollectionDao getShowCollectionDao();

    public void addShowCollectionDao(ShowCollectionDao showCollectionDao);

    public ShowDao getShowDao();

    public void setShowCollectionDao(ShowCollectionDao showCollectionDao);

    public void setShowDao(ShowDao showDao);
}
