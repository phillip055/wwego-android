package com.lelmaster.phillipsumetphong.wwego.dao;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vorathepsumetphong on 8/27/16.
 */
public class ShowDao {
    @SerializedName("cover_picture")
    private String cover_picture;

    @SerializedName("isPPV")
    private boolean PPV;

    @SerializedName("name")
    private String name;

    @SerializedName("show_type")
    private String show_type;

    @SerializedName("url")
    private String url;

    @SerializedName("links")
    private List<PartialLinkDao> partialLinkDaos;

    @SerializedName("created_date")
    private String created_date;

    @SerializedName("updated_at")
    private String updated_at;

    public String getCover_picture() {
        return cover_picture;
    }

    public void setCover_picture(String cover_picture) {
        this.cover_picture = cover_picture;
    }

    public boolean isPPV() {
        return PPV;
    }

    public void setPPV(boolean PPV) {
        this.PPV = PPV;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShow_type() {
        return show_type;
    }

    public void setShow_type(String show_type) {
        this.show_type = show_type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<PartialLinkDao> getPartialLinkDaos() {
        return partialLinkDaos;
    }

    public void setPartialLinkDaos(List<PartialLinkDao> partialLinkDaos) {
        this.partialLinkDaos = partialLinkDaos;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
