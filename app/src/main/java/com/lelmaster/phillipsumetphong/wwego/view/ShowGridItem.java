package com.lelmaster.phillipsumetphong.wwego.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.lelmaster.phillipsumetphong.wwego.R;

/**
 * Created by vorathepsumetphong on 8/27/16.
 */
public class ShowGridItem extends RelativeLayout {
    TextView name;
    ImageView imageView;
    TextView date;
    public ShowGridItem(Context context) {
        super(context);
        initInflate();
        initIntances();
    }

    public ShowGridItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initIntances();
    }

    public ShowGridItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initIntances();
    }



    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ShowGridItem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
        initIntances();
    }

    private void initIntances() {
        name = (TextView) findViewById(R.id.name);
        date = (TextView) findViewById(R.id.date);
        imageView = (ImageView) findViewById(R.id.img);
    }

    private void initInflate() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.show_grid_item, this);
    }

    public void setDate(String date) {
        this.date.setText(date);
    }
    public void setName(String name) {
        this.name.setText(name);
    }

    public void setImageView(String url){
        Glide.with(getContext())
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

}
