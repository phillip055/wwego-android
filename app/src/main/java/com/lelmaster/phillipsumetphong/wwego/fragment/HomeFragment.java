package com.lelmaster.phillipsumetphong.wwego.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.lelmaster.phillipsumetphong.wwego.R;
import com.lelmaster.phillipsumetphong.wwego.adapter.AllAdapter;
import com.lelmaster.phillipsumetphong.wwego.dao.ShowCollectionDao;
import com.lelmaster.phillipsumetphong.wwego.data.ShowType;
import com.lelmaster.phillipsumetphong.wwego.manager.http.HTTPManager;
import com.lelmaster.phillipsumetphong.wwego.view.SpacesItemDecoration;
import com.paginate.Paginate;

import retrofit.Call;
import retrofit.Callback;

/**
 * Created by vorathepsumetphong on 8/27/16.
 */
public class HomeFragment extends Fragment {
    private RecyclerView GridRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView.Adapter mAdapter;
    private int page = 0;
    private boolean isLoading = false;
    ShowType showType;


    public static HomeFragment newInstance(ShowType showType) {
        Bundle args = new Bundle();
        args.putSerializable("show_type",showType);
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.showType = (ShowType) getArguments().getSerializable("show_type");
        View rootView = inflater.inflate(R.layout.home_fragment, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView){
        GridRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerGridView);
        int spanCount = 2; // 3 columns
        int spacing = 50; // 50px
        boolean includeEdge = true;
        GridRecyclerView.addItemDecoration(new SpacesItemDecoration(spanCount, spacing, includeEdge));
        mLayoutManager = new GridLayoutManager(getContext(),2);
        GridRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = showType.getAdapter(getContext());
        GridRecyclerView.setAdapter(showType.getAdapter(getContext()));
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(0,20);
            }
        });

        Paginate.Callbacks callbacks = new Paginate.Callbacks() {
            @Override
            public void onLoadMore() {
                loadData((page)*20,((page)*20)+20);
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean hasLoadedAllItems() {
                return false;
            }
        };

        Paginate.with(GridRecyclerView, callbacks)
                .setLoadingTriggerThreshold(2)
                .addLoadingListItem(true)
                .build();

    }

    private void loadData(final int offset, int limit) {
        isLoading = true;
        Call<ShowCollectionDao> call;
        switch (showType){
            case ALL:
                call = HTTPManager.getInstance().getService().all(offset,limit);
                break;
            case Raw:
                call = HTTPManager.getInstance().getService().raw(offset,limit);
                break;
            case SmackDown:
                call = HTTPManager.getInstance().getService().smack_down(offset,limit);
                break;
            case NXT:
                call = HTTPManager.getInstance().getService().nxt(offset,limit);
                break;
            case Main_Event:
                call = HTTPManager.getInstance().getService().main_event(offset,limit);
                break;
            case PPV:
                call = HTTPManager.getInstance().getService().ppv(offset,limit);
                break;
            case UFC:
                call = HTTPManager.getInstance().getService().ufc(offset,limit);
                break;
            case TNC:
                call = HTTPManager.getInstance().getService().tna(offset,limit);
                break;
            default:
                call = HTTPManager.getInstance().getService().all(offset,limit);
        }

        call.enqueue(new Callback<ShowCollectionDao>() {
            @Override
            public void onResponse(retrofit.Response<ShowCollectionDao> response) {
                if (offset<=0) {
                    showType.getManager().setShowCollectionDao(response.body());
                } else {
                    showType.getManager().addShowCollectionDao(response.body());
                }
                mAdapter.notifyDataSetChanged();
                page ++ ;
                swipeRefreshLayout.setRefreshing(false);
                isLoading = false;
            }

            @Override
            public void onFailure(Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                isLoading = false;
                Log.e("http error", t.getMessage());
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
